from django.db import models
from django.utils import timezone

# Create your models here.
class Bacotan(models.Model):
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=300)
    time = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.name +" : " +  self.status

class Subscriber(models.Model):
    user = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=100)



