$(window).on('load',function(){
  if(jQuery.isReady) {
    $('#preload').fadeOut(2000, function() {
      $("#container").show("fold",2000);
      
    });
  }
})

$(document).ready(function(){

    $( function() {
    $( "#accordion" ).accordion({
       active: false,
       collapsible: true,
       heightStyle: "content"  
    });
  } );
    var check = 0;
    $("#changetheme").click(function(){
      if (check == 1){
        lightTheme(); check = 0;
      } else {
        darkTheme(); check = 1;
      }
    })

    function lightTheme() {
          $('#changetheme').html('Dark<br>Theme');
          $('#changetheme').animate({
            'background-color' : '#F36B31',
            'color' : 'black'
          })
          $('#header').animate({
          'background-color' : '#4F2A53'
        })
        $('.content').animate({
          'background-color' : '#B65142'
        })
        $('.content .info .inner').animate({
          'background-color' : '#F36B31'
        })
        $('.content .hub').animate({
          'background-color' : '#23144B'
        })
        $('#profpic').animate({
          'border-top-color' : '#F36B31'
        })
        $('h3.acc_header').animate({
          'background-color' : '#F36B31'
        })
        $('#accordion .inside').animate({
          'background-color' : '#4F2A53'
        })
    }

    function darkTheme() {
        $('#changetheme').html('Light<br>Theme');
        $('#changetheme').animate({
            'background-color' : '#233237',
            'color' : 'white'
          })
          $('#header').animate({
          'background-color' : '#3c555d'
        })
        $('.content').animate({
          'background-color' : '#18121E'
        })
        $('.content .info .inner').animate({
          'background-color' : '#243833'
        })
        $('.content .hub').animate({
          'background-color' : '#233237'
        })
        $('#profpic').animate({
          'border-top-color': '#243833'
        })
        $('h3.acc_header').animate({
          'background-color' : '#3c555d'
        })
        $('#accordion .inside').animate({
          'background-color' : '#18121E'
        })
    }
});