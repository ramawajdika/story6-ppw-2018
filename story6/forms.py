from django import forms


class UpdateBacotan(forms.Form):
    attrs = {"class": "form-control"}

    name = forms.CharField(label="name", max_length=100, required=False, widget=forms.TextInput(attrs=attrs))
    status = forms.CharField(label="status", max_length=300, required=True, widget=forms.Textarea(attrs=attrs))

class UpdateSubscriber(forms.Form):
    attrs = {"class": "form-control"}

    user = forms.CharField(label="username", max_length=100, required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label="email", max_length=300, required=True, widget=forms.EmailInput(attrs=attrs))
    password= forms.CharField(label="password", max_length=100, required=True, widget=forms.PasswordInput(attrs=attrs))