from django.shortcuts import render
from django.http import HttpResponseRedirect
from .views import *
from .models import *
from .forms import *
from django.urls import reverse
import requests
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
import  json


# Create your views here.

response = {}

def view_register(request):
    response['subscriber_form'] = UpdateSubscriber
    return render(request, 'register.html', response)

def regis(request):
    form = UpdateSubscriber(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        user = request.POST['user']
        email = request.POST['email']
        password = request.POST['password']
        response['user'] = user
        response['email'] = email
        response['password'] = password
        subs = Subscriber(user=response['user'], email=response['email'],password=response['password'])
        subs.save()
        return HttpResponse('OK')

def delete_user(request):
    email = request.GET.get('email',None)
    Subscriber.objects.get(email=email).delete()
    return HttpResponse('OK')


def check_password(request):
    exist = False
    if (request.method == 'GET'):
        password = request.GET.get('pass',None)
        print(password)
        emails = request.GET.get('email', None)

        exists = Subscriber.objects.get(email=emails)
        print((exists.password))
        if (exists.password == password):
            exist = True


    return JsonResponse({'exists': exist})

def balikin_subscriber(request):
    model = Subscriber.objects.values()
    model = list(model)
    return JsonResponse({'data': model})

def check_email(request):
    exists = False
    if(request.method == 'POST'):
        email = request.POST['email']
        exists = Subscriber.objects.filter(email=email).exists()

    return JsonResponse({'exists':exists})

def view_profile(request):
    return render(request,'aboutme.html')

# Create your views here.
def view_home(request):
    bacotans = Bacotan.objects.all()[::-1]
    response['bacotan_display'] = bacotans
    response['bacotan_form'] = UpdateBacotan
    return render(request, 'base.html', response)

def view_books(request):
    return render(request,'books.html')

def balik(request):
    return HttpResponseRedirect(reverse('home'))


def send(request):
    form = UpdateBacotan(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['status'] = request.POST['status']

        status = Bacotan(name=response['name'], status=response['status'])
        status.save()
        return HttpResponseRedirect(reverse('home'))
    else:
        return HttpResponseRedirect(reverse('home'))


def ambil(request,judul):
    print("woi")
    url = "https://www.googleapis.com/books/v1/volumes?q=" + judul
    books = requests.get(url).json()
    return JsonResponse(books)


