from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time


# Create your tests here.
class Story6UnitTest(TestCase):
    def setUp(self):
        return Bacotan.objects.create(name="fulan", status="beli beras nih")

    def test_scrum_debate_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_scrum_debate_url_func_working(self):
        found = resolve(reverse('home'))
        self.assertEqual(found.func, view_home)

    def test_scrum_debate_form_is_valid(self):
        new_form = UpdateBacotan(
            data={'name': 'fulan', 'status': 'beli beras nih'})
        self.assertTrue(new_form.is_valid())

    def test_scrum_debate_form_has_css_class(self):
        new_form = UpdateBacotan()
        self.assertIn('class="form-control', new_form.as_p())

    def test_scrum_debate_form_validation_for_blank_and_offlimit_input(self):
        new_form = UpdateBacotan(data={'name': '', 'status': ''})
        self.assertTrue(new_form.fields['status'].max_length == 300)
        self.assertFalse(new_form.is_valid())

    def test_scrum_debate_model_can_create_new_status(self):
        # create new status model
        count_bacotan = Bacotan.objects.all().count()
        self.assertEqual(count_bacotan, 1)

    def test_scrum_debate_content_is_valid(self):
        new_status = self.setUp()
        self.assertEqual(new_status.name, 'fulan')
        self.assertEqual(new_status.status, 'beli beras nih')

    def test_scrum_debate_model_can_print(self):
        new_status = self.setUp()
        self.assertEqual('fulan : beli beras nih', new_status.__str__())

    def test_scrum_debate_page_contains_correct_html(self):
        response = self.client.get('')
        self.assertContains(response, '<h1>SCRUM DEBATE</h1>')
        self.assertContains(response, '<div class="jumbotron">')
        self.assertContains(response, 'button id="kirim" type="submit" class="btn btn-success"')

    def test_scrum_debate_post_success_and_render_result(self):
        response_status = Client().post(reverse('send'), {'name': 'fulan', 'status': 'beli beras nih'})
        self.assertEqual(response_status.status_code, 302)
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('fulan', html_response)

    # --------------------------- tests for challenge ---------------------------

    def test_scrum_debate_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_scrum_debate_profile_url_func_working(self):
        found = resolve(reverse('profile'))
        self.assertEqual(found.func, view_profile)

    def test_scrum_debate_profile_page_contains_correct_html(self):
        response = self.client.get('/profile/')
        self.assertContains(response, 'Ramawajdi Kanishka Anwar')
        self.assertContains(response, '12 January 1999')
        self.assertContains(response, '1706043595')

    # ------------------------ Unit test story 9 ---------------------------
    def test_books_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_books_url_func_working(self):
        found = resolve(reverse('books'))
        self.assertEqual(found.func, view_books)

#------------------------tests for story10 ----------------------------------------
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_func_working(self):
        found = resolve(reverse('register'))
        self.assertEqual(found.func, view_register)

    def test_register_form_is_valid(self):
        new_form = UpdateSubscriber(
            data={'user': 'fulan', 'email': 'xxx@gmail.com','password':'wololo'})
        self.assertTrue(new_form.is_valid())

    def test_register_post_success_and_render_result(self):
        response_status = Client().post(reverse('regis'),{'user': 'fulan', 'email': 'xxx@gmail.com','password':'wololo'})
        self.assertEqual(response_status.status_code, 200)



class Story6FunctionalTest(StaticLiveServerTestCase):
    '''
    for test the real web, use the localhost:8000 , the live_server_url is tested
    to maintain the real website
    '''

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(10);
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        time.sleep(2)
        name = selenium.find_element_by_id('id_name')
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('kirim')
        name.send_keys('Kak Pewe')
        status.send_keys('h e y  t a y o')

        submit.send_keys(Keys.RETURN)
        self.assertIn("Kak Pewe", self.selenium.page_source)
        self.assertIn("h e y  t a y o", self.selenium.page_source)

        time.sleep(2)

    def test_check_name_on_profile(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/profile/')
        time.sleep(2)

        self.assertIn("Ramawajdi Kanishka Anwar", self.selenium.page_source)
        self.assertIn("081298899112", self.selenium.page_source)
        self.assertIn("12 January 1999", self.selenium.page_source)
        self.assertIn("12 January 1999", self.selenium.page_source)
        self.assertIn("ramawajdika", self.selenium.page_source)

    def test_from_profile_can_go_back_to_home(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/profile/')

        submit = selenium.find_element_by_id('butt')
        time.sleep(2)
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn("SCRUM DEBATE", self.selenium.page_source)

    def test_back_button_color(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/profile/')
        time.sleep(2)
        ret = selenium.find_element_by_id('butt').get_attribute("class")
        self.assertIn("btn-success", ret)

    def test_submit_button_color(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        time.sleep(2)
        ret = selenium.find_element_by_id('kirim').get_attribute("class")
        self.assertIn("btn-success", ret)

    # ------------------------- functional test for story 8 ----------------------

    def test_accordion_contain_true_data(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/profile/')
        time.sleep(2)
        self.assertIn("Current Activities", self.selenium.page_source)
        self.assertIn("Experiences", self.selenium.page_source)
        self.assertIn("Achievements", self.selenium.page_source)

    def test_change_theme_check_colors(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/profile/')
        acc = selenium.find_element_by_id('changetheme')
        time.sleep(6)
        acc.click()
        time.sleep(2)
        head = selenium.find_element_by_id('header')
        inner = selenium.find_element_by_id('inners')
        hub = selenium.find_element_by_id('hubs')
        profpic = selenium.find_element_by_id('profpic')
        h3 = selenium.find_element_by_class_name('acc_header')
        accbox = selenium.find_element_by_id('accordion').find_element_by_class_name('inside')
        color = head.value_of_css_property('background-color')
        color2 = inner.value_of_css_property('background-color')
        color3 = hub.value_of_css_property('background-color')
        color4 = profpic.value_of_css_property('border-top-color')
        color5 = h3.value_of_css_property('background-color')
        color6 = accbox.value_of_css_property('background-color')

        self.assertEqual("rgba(60, 85, 93, 1)", color)
        self.assertEqual("rgba(36, 56, 51, 1)", color2)
        self.assertEqual("rgba(35, 50, 55, 1)", color3)
        self.assertEqual("rgba(36, 56, 51, 1)", color4)
        self.assertEqual("rgba(60, 85, 93, 1)", color5)
        self.assertEqual("rgba(24, 18, 30, 1)", color6)

#-------------------

    # def test_favorite_counter_go_up(self):
    #     selenium = self.selenium
    #     selenium.get(self.live_server_url + '/books/')
    #     time.sleep(2)
    #     cout = selenium.find_element_by_id('taa')
    #     self.assertIn("Jumlah buku favorit : 0",cout)
    #     star = selenium.find_element_by_id('mcaasXqHSFIC')
    #     star.click()
    #     time.sleep(5)
    #     self.assertIn("Jumlah buku favorit : 1", cout)
    #     star.click()
    #     time.sleep(2)
    #     self.assertIn("Jumlah buku favorit : 0", cout)
