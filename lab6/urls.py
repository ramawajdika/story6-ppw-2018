"""lab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.urls import include
from story6.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', view_home, name="home"),
    path('send/', send, name="send"),
    path('balik/', balik, name="balik"),
    path('profile/', view_profile, name="profile"),
    path('books/', view_books, name="books"),
    path('ambil/<str:judul>', ambil,name ="ambil"),
    path('register/',view_register,name="register"),
    path('checkemail/',check_email),
    path('affirmative/',regis,name="regis"),
    path('getalluser/',balikin_subscriber,name="balikin"),
    path('checkpassword/',check_password,name="checkpassword"),
    path('deleteuser/',delete_user,name="deleteuser")


]
