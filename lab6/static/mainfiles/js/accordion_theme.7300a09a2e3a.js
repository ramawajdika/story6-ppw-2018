$(document).ready(function(){

    $( function() {
    $( "#accordion" ).accordion({
      active: false,
      collapsible: true  
    });
  } );
    var check = 0;
    $("#changetheme").click(function(){
      if (check == 1){
        lightTheme(); check = 0;
      } else {
        darkTheme(); check = 1;
      }
    })

    function lightTheme() {
          $('#header').css({
          'background-color' : '#4F2A53'
        })
        $('.content').css({
          'background-color' : '#B65142'
        })
        $('.content .info .inner').css({
          'background-color' : '#F36B31'
        })
        $('.content .hub').css({
          'background-color' : '#23144B'
        })
        $('#profpic').css({
          'border-top-color' : '#F36B31'
        })
        $('h3.acc_header').css({
          'background-color' : '#F36B31'
        })
        $('#accordion .inside').css({
          'background-color' : '#4F2A53'
        })
    }

    function darkTheme() {
          $('#header').css({
          'background-color' : '#3c555d'
        })
        $('.content').css({
          'background-color' : '#18121E'
        })
        $('.content .info .inner').css({
          'background-color' : '#243833'
        })
        $('.content .hub').css({
          'background-color' : '#233237'
        })
        $('#profpic').css({
          'border-top-color': '#243833'
        })
        $('h3.acc_header').css({
          'background-color' : '#3c555d'
        })
        $('#accordion .inside').css({
          'background-color' : '#18121E'
        })
    }
});